package com.example.davidbernal.sharedpref

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var editTextName: EditText
    lateinit var editTextApellido: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editTextName = findViewById(R.id.editTextName)
        editTextApellido = findViewById(R.id.editTextApellido)

        recuperarDatos()

        findViewById<Button>(R.id.buttonG).setOnClickListener{
            guardarDatos()
        }
    }

    private fun recuperarDatos(){
        val mypref = getSharedPreferences("mypref", Context.MODE_PRIVATE)
        val name = mypref.getString("name","")
        val apellido = mypref.getString("apellido","")

        editTextName.setText(name)
        editTextApellido.setText(apellido)
    }

    private fun guardarDatos() {
        if (editTextName.text.isEmpty()){
            editTextName.error = "Por favor ingresa un nombre"
            return
        }

        if (editTextApellido.text.isEmpty()){
            editTextApellido.error = "Porfavor ingresa un apellido"
            return
        }

        val mypref = getSharedPreferences("mypref", Context.MODE_PRIVATE)

        val editor = mypref.edit()

        editor.putString("name",editTextName.text.toString())
        editor.putString("apellido",editTextApellido.text.toString())

        editor.apply()

        Toast.makeText(this,"Datos Guardados", Toast.LENGTH_LONG).show()

    }
}
